<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Article extends Model
{
    //írható fieldek
    protected $fillable=[
      'title','lead','content','published_on'  
    ];
    //hivható legyen a modellen egy szűrt lekérés
    public function scopePublished($query){
        return $query->where('published_on','<=',Carbon::now());
    }
    //egy tulajdonság beállítása tipusra pl published_on legyen carbon példány
    public function getPublishedOnAttribute($value){
        return Carbon::createFromFormat('Y-m-d H:i:s',$value);
    }
    
}
