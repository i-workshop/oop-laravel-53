<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
class PageController extends Controller {

    function about() {
        $firstname = 'György';
        $lastname = 'Horváth';
        $friends = ['Tercsi','Fercsi','Kata','Klára'];
//        return view('teszt',[
//            'firstname'=>$firstname,
//            'lastname'=>$lastname,
//            'friends'=>$friends
//        ]);
        return view('about',compact('firstname','lastname','friends'));
    }

    function contact() {
        return view('contact');
    }
    
    function sendEmail(Request $request){
        $rules = [
            'name' => 'required|min:3',
            'email'=> 'required|email',
            'message'=>'required|min:20|max:500'
        ];
        $this->validate($request, $rules);
        $subject='Message from webpage';
        //küldjük el az emailt
         Mail::send('emails.contact',['msg'=>$request->input('message')], function ($m) use($subject,$request) {
            $m->from($request->input('email'), $request->input('name'));
            $m->to('hgy@ruander.hu', 'Webpage support')->subject($subject);
        });
        return redirect('contact/thankyou');
        echo var_export($request->all(),true);
        die();
    }
}
