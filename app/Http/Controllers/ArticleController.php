<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Article;
use Carbon\Carbon;
use Faker;

class ArticleController extends Controller {

    function index() {
        $articles = Article::published()->orderBy('published_on','desc')->get();
        return view('articles.index', compact('articles'));
    }
    function show($id){
        $article=Article::find($id);
        return view('articles.show',compact('article'));
    }
    function createTen() {//db be feltölt 10 véletlenszerű (dummy) cikket
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $data = [
                'title' => $faker->sentence(6, true),
                'lead' => $faker->paragraph(3, true),
                'content' => $faker->paragraphs(3, true),
                'published_on' => $faker->dateTimeBetween('-1 week', '+1 week'),
            ];
//            echo '<pre>'.var_export($data,true).'</pre>';
            $article = new Article($data);
            $article->save();
        }
        return redirect('articles');
    }

    function createOne() {
        $article = new Article([
            'title' => 'My new Article',
            'lead' => 'My new lead',
            'content' => 'My fancy new content',
            'published_on' => Carbon::now(),
        ]);
        $article->save();
        return redirect('articles');
    }

}
