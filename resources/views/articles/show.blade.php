@extends('layouts.master')

@section('title','Articles list')

@section('content')
<div class="row">
    <article class="col-xs-12">
        <header>
            <a href="{{ URL::previous() }}">back to articles</a>
            <h1>{{ $article->title }}</h1>
            <time class="text-info" datetime="{{ $article->published_on }}">Published on: {{ $article->published_on }}</time>
        </header>
        <blockquote class="alert-info">
            {{ $article->lead }}
        </blockquote>     
        <section>
            {{ $article->content }}
        </section>
    </article>
</div>
@stop