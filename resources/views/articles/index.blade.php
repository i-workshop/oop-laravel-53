@extends('layouts.master')

@section('title','Articles list')

@section('content')
<div class="row">
    @if(count($articles)>0)
    <ul class="col-xs-12">
        @foreach($articles as $article)
        <li><article>
                <h1><a href="{{ url('articles',$article->id) }}">{{ $article->title }}</a></h1>
                <div>
                    {{ $article->lead }}
                </div>
                <time datetime="{{ $article->published_on }}">Published on: {{ $article->published_on }}</time>
            </article></li>
        @endforeach
    </ul>
    @else
    <div class="col-xs-12 alert alert-info">No articles...</div>
    @endif
</div>
@stop