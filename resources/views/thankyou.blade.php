@extends('layouts.master')

@section('title','Thank you page')

@section('content')
<div class="row">
    <div class="col-xs-12 alert alert-success">
        <h1>Your message sent! Thank you!</h1>
    </div>
</div>
@stop