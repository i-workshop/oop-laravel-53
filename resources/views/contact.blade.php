@extends('layouts.master')
@section('title','Contact')
@section('content')
<h1>Contact Page!</h1>

@if( $errors->any() )
<div class="alert alert-danger">
    <ul>
        @foreach($errors->all() as $error )
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div> 
@endif


{!! Form::open(['url'=>'contact']) !!}

<div class="form-group">
    <div class="row">
        <div class="col-md-2 col-xs-12">
            {!! Form::label('name', 'Name', ['class' => '']) !!}
        </div>
        <div class="col-md-10 col-xs-12">
            {!! Form::text('name', '' ,['class' => 'form-control col-xs-12' , 'placeholder'=>'John Doe']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12">
            {!! Form::label('email', 'E-Mail Address', ['class' => '']) !!}
        </div>
        <div class="col-md-10 col-xs-12">
            {!! Form::text('email', '' ,['class' => 'form-control col-xs-12' , 'placeholder'=>'example@email.com']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 col-xs-12">
            {!! Form::label('message', 'Message', ['class' => '']) !!}
        </div>
        <div class="col-md-10 col-xs-12 ">
            {!! Form::textarea('message', '' ,['class' => 'form-control col-xs-12' , 'placeholder'=>'blabla...']) !!}
        </div>
    </div>


    {!! Form::submit('Send !',['class'=>'btn btn-default']) !!}
</div>
{!! Form::close() !!}
@endsection