<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');
Route::get('about','PageController@about')->name('about');
Route::get('contact','PageController@contact')->name('contact');
Route::post('contact','PageController@sendEmail');
Route::get('contact/thankyou',function(){
    return view('thankyou');
});
//articles

Route::get('articles','ArticleController@index')->name('articles');
Route::get('articles/createone','ArticleController@createOne');
Route::get('articles/createten','ArticleController@createTen');
Route::get('articles/{id}','ArticleController@show');